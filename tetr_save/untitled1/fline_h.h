#ifndef FLINE_H
#define FLINE_H

#include "figure.h"

class fLine_h : public  Figure
{
    Q_OBJECT

public:
    fLine_h(QWidget *parent=0);
    virtual bool BoamR();
    virtual bool BoamL();
    virtual void Trans();
public slots:
    virtual void MoveB();
};

#endif // FLINE_H

