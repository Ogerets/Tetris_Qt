#ifndef FCROSS
#define FCROSS

#include "figure.h"

class fCross : public Figure
{
    Q_OBJECT

public:
    fCross(QWidget *parent =0);
    virtual void Trans();
    virtual bool BoamR();
    virtual bool BoamL();
public slots:
    virtual void MoveB();
};

#endif // FCROSS

