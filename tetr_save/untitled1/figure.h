#ifndef FIGURE
#define FIGURE

#include <QWidget>
#include "Cube.h"

class Figure: public QWidget
{
    Q_OBJECT
public:
    Figure(QWidget *parent = 0) :
        QWidget(parent)
    { }

    Cube **m;
    int pol;
    int posH;
    int posX;
    int posY;
    int posV;
    //void setPosX(int x){posX=x;}
   // void setPosY(int y){posY=y;}
    //int getPosX(){return posX;}
    //int getPosY(){return posY;}
    virtual bool BoamR() = 0;
    virtual bool BoamL() = 0;
    virtual void Trans() = 0;
    virtual void MoveB() = 0;
//private:
//    int posX;
//    int posY;
};

#endif // FIGURE

