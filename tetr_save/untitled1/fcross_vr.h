#ifndef FCROSS_VR
#define FCROSS_VR

#include "figure.h"

class fCross_vr : public Figure
{
    Q_OBJECT

public:
    fCross_vr(QWidget *parent =0);
    virtual void Trans();
    virtual bool BoamR();
    virtual bool BoamL();
public slots:
    virtual void MoveB();
};

#endif // FCROSS_VR

