#ifndef FSNAKEL
#define FSNAKEL

#include "figure.h"

class fSnakeL : public  Figure
{
    Q_OBJECT

public:
    fSnakeL(QWidget *parent=0);
    virtual bool BoamR();
    virtual bool BoamL();
    virtual void Trans();
public slots:
    virtual void MoveB();
};

#endif // FSNAKEL

