#ifndef FHORSEL_HR
#define FHORSEL_HR

#include "figure.h"

class fHorseL_hr : public  Figure
{
    Q_OBJECT

public:
    fHorseL_hr(QWidget *parent=0);
    virtual bool BoamR();
    virtual bool BoamL();
    virtual void Trans();
public slots:
    virtual void MoveB();
};

#endif // FHORSEL_HR

