#ifndef FLINE
#define FLINE

#include "figure.h"

class fLine : public  Figure
{
    Q_OBJECT

public:
    fLine(QWidget *parent=0);
    virtual bool BoamR();
    virtual bool BoamL();
    virtual void Trans();
public slots:
    virtual void MoveB();
};

#endif // FLINE

