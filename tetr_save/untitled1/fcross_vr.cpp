#include "fCross_vr.h"
#include "genwindow.h"

extern GenWindow *win;

fCross_vr::fCross_vr(QWidget *parent) : Figure(parent)
{
    posX=100;
    posY=-75;
    posV=-1;
    posH=4;
    setGeometry(posX,posY, 50, 75);
    m = (Cube **)malloc(4*sizeof(Cube));
    for (int i=0;i<4;i++){
        m[i] = new Cube(this);
        m[i]->show();
    }
    m[0]->posX=0;
    m[0]->posY=0;
    m[1]->posX=0;
    m[1]->posY=25;
    m[2]->posX=25;
    m[2]->posY=25;
    m[3]->posX=0;
    m[3]->posY=50;
    m[0]->setGeometry(m[0]->posX,m[0]->posY,25,25);
    m[1]->setGeometry(m[1]->posX,m[1]->posY,25,25);
    m[2]->setGeometry(m[2]->posX,m[2]->posY,25,25);
    m[3]->setGeometry(m[3]->posX,m[3]->posY,25,25);
}

void fCross_vr::MoveB()
{
    posY++;

    for (int i=-2;i<16;i++)
    if (posY==i*25) {posV=i+1; break;}

    setGeometry(posX,posY, width(), height());

    if ((win->q[posH][posV+3]>=0) || (win->q[posH+1][posV+2]>=0) || (posV==14))
    {
         //win->qMessageBox::information(this,"win->qString::number(posH)","win->qString::number(posV)");
         QObject::disconnect(win->tmr,SIGNAL(timeout()),this,SLOT(MoveB()));
         win->V[posH]=win->V[posH]+3;
         win->V[posH+1]++;

         if (posV<=0) {QMessageBox::information(this,"Tetris","Game Over!"); win->close(); return;}

         win->locat(0,0);

         win->locat(0,1);

         win->locat(1,1);

         win->locat(0,2);

         delete(this);
         win->nweC();
     }
}

void fCross_vr::Trans()
{
    if ((posH>0) && (win->q[posH-1][posV+2]==-1))
    {
        QObject::disconnect(win->tmr,SIGNAL(timeout()),this,SLOT(MoveB()));
        int x=posX-25;
        int y=posY+25;
        int h=posH-1;
        int v=posV+1;
        delete(this);
        win->f = new fCross_hd(win);
        win->f->show();
        win->f->posX=x;
        win->f->posY=y;
        win->f->posH=h;
        win->f->posV=v;
        win->f->setGeometry(x,y,win->f->width(),win->f->height());
        QObject::connect(win->tmr,SIGNAL(timeout()),win->f,SLOT(MoveB()));
    }
}

bool fCross_vr::BoamR()
{
    if ((win->q[posH+1][posV+3]<0) && (win->q[posH+2][posV+2]<0))  return 1;
    else return 0;
}

bool fCross_vr::BoamL()
{
    if ((win->q[posH-1][posV+3]<0) && (win->q[posH-1][posV+2]<0))  return 1;
    else return 0;
}


