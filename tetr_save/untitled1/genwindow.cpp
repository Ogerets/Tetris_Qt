#include "genwindow.h"
#include <QTime>

GenWindow::GenWindow(QWidget *parent):QWidget(parent)
{
    qsrand(QTime::currentTime().msec());
    for (int t=0;t<10;t++)
    {   V[t]=0;
        for (int i=0;i<=16;i++){
            H[i]=0;
            q[t][i]=-1;
        }
    }

    setGeometry(200, 200, 249, 400);
    num=0;

    tmr = new QTimer(this);
    tmr->setInterval(10);
    tmr->start();
    nweC();
}

void GenWindow::locat(int x, int y)
{
    num++;
    if (num==1) b = (Cube **)malloc(sizeof(Cube)); else b = (Cube **)realloc(b,num*sizeof(Cube));
    b[num-1] = new Cube(this);
    b[num-1]->show();
    //if ((x==1) && (y==0)) QMessageBox::information(this,"QString::number(posH)","QString::number(posV)");
    b[num-1]->posX = f->posX+25*x;
    b[num-1]->posY = f->posY+25*y;
    b[num-1]->setGeometry(b[num-1]->posX,b[num-1]->posY,25,25);
    //QMessageBox::information(this,QString::number(f->posH),QString::number(f->posV), QString::number(num-1));
    q[f->posH+x][f->posV+y]=num-1;
    H[f->posV+y]++;

    if (H[f->posV+y]==10) {destr(f->posV+y);}
}

void GenWindow::nweC()
{
    type = qrand()%15;
    //type = 11;
    if (type==0)
        f = new fCube(this);
    else if (type==1)
        f = new fLine(this);
    else if (type==2)
        f = new fSnakeL(this);
    else if (type==3)
        f = new fSnakeR(this);
    else if (type==4)
        f = new fHorseL(this);
    else if (type==5)
        f = new fLine_h(this);
    else if (type==6)
        f = new fSnakeL_h(this);
    else if (type==7)
        f = new fSnakeR_h(this);
    else if (type==8)
        f = new fHorseL_hr(this);
    else if (type==9)
        f = new fHorseL_vl(this);
    else if (type==10)
        f = new fHorseL_hl(this);
    else if (type==11)
        f = new fCross(this);
    else if (type==12)
        f = new fCross_vr(this);
    else if (type==13)
        f = new fCross_hd(this);
    else if (type==14)
        f = new fCross_vl(this);
    f->show();
    QObject::connect(tmr,SIGNAL(timeout()),f,SLOT(MoveB()));

      // QMessageBox::information(this,QString::number(win->q[posH][posV]),QString::number(win->q[posH][posV]));
}

/*void GenWindow::createf(int x, int y, int v, int h)
{
    delete(f);
    //type = 4;
    if (type==0)
        f = new fCube(this);
    else if (type==1)
        f = new fLine(this);
    else if (type==2)
        f = new fSnakeL(this);
    else if (type==3)
        f = new fSnakeR(this);
    else if (type==4)
        f = new fHorseL(this);
    else if (type==5)
        f = new fLine_h(this);
    else if (type==6)
        f = new fSnakeL_h(this);
    else if (type==7)
        f = new fSnakeR_h(this);
    else if (type==8)
        f = new fHorseL_hr(this);
    else if (type==9)
        f = new fHorseL_vl(this);
    else if (type==10)
        f = new fHorseL_hl(this);
    f->posX=x;
    f->posY=y;
    f->posH=h;
    f->posV=v;
    f->setGeometry(x,y,f->width(),f->height());
    f->show();
    QObject::connect(tmr,SIGNAL(timeout()),f,SLOT(MoveB()));

      // QMessageBox::information(this,QString::number(win->q[posH][posV]),QString::number(win->q[posH][posV]));
}*/

void GenWindow::keyPressEvent(QKeyEvent *event)
{
    key = event->key();
    if ((key == Qt::Key_Right) && (f->posX+1<width()-f->width())) /*&& (q[f.posH+2][f->posV]<0)*///)
    {
       if (f->BoamR()==1)
        {
            f->posH++;
            f->posX= f->posX+25;
            f->setGeometry(f->posX,f->posY,f->width(),f->height());
        }
    }
    else if (((key == Qt::Key_Left) || (key == Qt::Key_A)) && (f->posX-1>0))
    {
        //QMessageBox::information(this, "QString::number(f[num-1]->posV)",QString::number(boamL()));
        if  (f->BoamL()==1)
        {
            f->posH--;
            f->posX= f->posX-25;
            f->setGeometry(f->posX,f->posY,f->width(),f->height());
        }
    }
    else if ((key == Qt::Key_Down) || (key == Qt::Key_S))
        tmr->setInterval(1);
    else if ((key == Qt::Key_Up) || (key == Qt::Key_W))
        f->Trans();
}

void GenWindow::keyReleaseEvent(QKeyEvent* event)
{
    if ((key == Qt::Key_Down) || (key == Qt::Key_S) && (event->key()==key))
        tmr->setInterval((10));
}

void GenWindow::destr(int posV)
{
    H[posV]=0;
    //QMessageBox::information(this,"QString::number(posH)",QString::number(H[f->posV]));
    for (int i=0;i<10;i++)
    {
        delete(b[q[i][posV]]);

        //b[q[i][posV]]->setVisible(false);

        q[i][posV]=-1;
        V[i]--;

        mdown(i,posV-1);

    }

}

void GenWindow::mdown(int posH, int posV)
{

    // QMessageBox::information(this, QString::number(H[posV]),QString::number(H[posV+1]));
    if (posV==0) return;
    if (q[posH][posV]!=-1)
    {
        b[q[posH][posV]]->posY=b[q[posH][posV]]->posY+25;

        b[q[posH][posV]]->setGeometry(b[q[posH][posV]]->posX,b[q[posH][posV]]->posY,25,25);

        q[posH][posV+1] = q[posH][posV];

        q[posH][posV]=-1;

        H[posV]--;

        H[posV+1]++;
    }

  mdown(posH,posV-1);

}
