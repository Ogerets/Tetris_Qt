#ifndef FSNAKER
#define FSNAKER

#include "figure.h"

class fSnakeR : public Figure
{
    Q_OBJECT

public:
    fSnakeR(QWidget *parent=0);
    virtual bool BoamR();
    virtual bool BoamL();
    virtual void Trans();
public slots:
    virtual void MoveB();
};

#endif // FSNAKER

