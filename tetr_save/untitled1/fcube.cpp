#include "fCube.h"
#include "genwindow.h"

extern GenWindow *win;

fCube::fCube(QWidget *parent) : Figure(parent)
{
    posX=100;
    posY=-50;
    posV=-1;
    posH=4;
    setGeometry(posX,posY, 50, 50);
    m = (Cube **)malloc(4*sizeof(Cube));
    for (int i=0;i<4;i++){
        m[i] = new Cube(this);
        m[i]->show();
    }
    m[0]->posX=0;
    m[0]->posY=0;
    m[1]->posX=25;
    m[1]->posY=0;
    m[2]->posX=0;
    m[2]->posY=25;
    m[3]->posX=25;
    m[3]->posY=25;
    m[0]->setGeometry(m[0]->posX,m[0]->posY,25,25);
    m[1]->setGeometry(m[1]->posX,m[1]->posY,25,25);
    m[2]->setGeometry(m[2]->posX,m[2]->posY,25,25);
    m[3]->setGeometry(m[3]->posX,m[3]->posY,25,25);
}

void fCube::MoveB()
{
    posY++;

    for (int i=-2;i<16;i++)
    if (posY==i*25) {posV=i+1; break;}

    setGeometry(posX,posY, width(), height());

    if ((win->q[posH][posV+2]>=0) || (win->q[posH+1][posV+2]>=0) || (posV==15))
    {
         //win->qMessageBox::information(this,"win->qString::number(posH)","win->qString::number(posV)");
         QObject::disconnect(win->tmr,SIGNAL(timeout()),this,SLOT(MoveB()));
         win->V[posH]=win->V[posH]+2;
         win->V[posH+1]=win->V[posH+1]+2;

         if (posV<=0) {QMessageBox::information(this,"Tetris","Game Over!"); win->close(); return;}

         win->locat(0,0);

         win->locat(1,0);

         win->locat(0,1);

         win->locat(1,1);

         delete(this);
         win->nweC();
     }
}

bool fCube::BoamR()
{
    if (/*(win->q[posH+2][posV+1]<0) && */(win->q[posH+2][posV+2]<0)) return 1;
    else return 0;
}

bool fCube::BoamL()
{
    if (/*(win->q[posH-1][posV+1]<0) && */(win->q[posH-1][posV+2]<0)) return 1;
    else return 0;
}
