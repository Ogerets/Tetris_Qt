#include "fHorseL_hr.h"
#include "genwindow.h"

extern GenWindow *win;

fHorseL_hr::fHorseL_hr(QWidget *parent) : Figure(parent)
{
    posV=-1;
    posX = 100;
    posH = 4;
    m = (Cube **)malloc(4*sizeof(Cube));
    posY = -50;
    setGeometry(posX,posY, 75, 50);
    for (int i=0;i<4;i++){
        m[i] = new Cube(this);
        m[i]->show();
    }
    m[0]->posX=50;
    m[0]->posY=0;
    m[0]->setGeometry(m[0]->posX,m[0]->posY,25,25);

    m[1]->posX=50;
    m[1]->posY=25;
    m[1]->setGeometry(m[1]->posX,m[1]->posY,25,25);

    m[2]->posX=25;
    m[2]->posY=25;
    m[2]->setGeometry(m[2]->posX,m[2]->posY,25,25);

    m[3]->posX=0;
    m[3]->posY=25;
    m[3]->setGeometry(m[3]->posX,m[3]->posY,25,25);
}


void fHorseL_hr::MoveB()
{
    posY++;

    for (int i=-2;i<16;i++)
    if (posY==i*25) {posV=i+1; break;}

    setGeometry(posX,posY, width(), height());

    //QMessageBox::information(this,QString::number(win->f->posX),QString::number(win->f->posY));
    if ((win->q[posH][posV+2]>=0) || (win->q[posH+1][posV+2]>=0) || (win->q[posH+2][posV+2]>=0) || (posV==15))
    {
         QObject::disconnect(win->tmr,SIGNAL(timeout()),this,SLOT(MoveB()));
         win->V[posH]++;
         win->V[posH+1]++;
         win->V[posH+2] = win->V[posH+2]+2;

         if (posV<=0) {QMessageBox::information(this,"Tetris","Game Over!"); win->close(); return;}

         win->locat(2,0);

         win->locat(2,1);

         win->locat(1,1);

         win->locat(0,1);

         delete(this);
         win->nweC();
    }
}

void fHorseL_hr::Trans()
{
    if (win->q[posH+2][posV+1]==-1)
    {
        QObject::disconnect(win->tmr,SIGNAL(timeout()),this,SLOT(MoveB()));
        int x=posX;
        int y=posY-25;
        int h=posH;
        int v=posV-1;
        delete(this);
        win->f = new fHorseL_vl(win);
        win->f->show();
        win->f->posX=x;
        win->f->posY=y;
        win->f->posH=h;
        win->f->posV=v;
        win->f->setGeometry(x,y,win->f->width(),win->f->height());
        QObject::connect(win->tmr,SIGNAL(timeout()),win->f,SLOT(MoveB()));
    }
}

bool fHorseL_hr::BoamR()
{
    if (win->q[posH+3][posV+2]<0) return 1;
    return 0;
}

bool fHorseL_hr::BoamL()
{
   if (win->q[posH-1][posV+2]<0) return 1;
   return 0;
}


