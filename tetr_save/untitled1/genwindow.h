#ifndef GENWINDOW
#define GENWINDOW

#include <QTimer>
#include <QKeyEvent>
#include <QMessageBox>
#include <QWidget>
#include "Figure.h"
#include "fcube.h"
#include "fline.h"
#include "fsnakel.h"
#include "fsnaker.h"
#include "fhorsel.h"
#include "fline_h.h"
#include "fsnakel_h.h"
#include "fsnaker_h.h"
#include "fhorsel_hr.h"
#include "fhorsel_vl.h"
#include "fhorse_hl.h"
#include "fcross.h"
#include "fcross_vr.h"
#include "fcross_hd.h"
#include "fcross_vl.h"

class Figure;

//struct web
//{
//    short int number;
//};

class GenWindow : public QWidget
{
    Q_OBJECT

public:
    GenWindow(QWidget *parent=0);
    Cube **b;
    Figure *f;
    QTimer *tmr;

    int num;
    int type;
    //void createf(int x,int y, int v, int h);
    void nweC();
    void locat(int x, int y);
    bool boamU();
    void destr(int posV);
    void mdown(int posH, int posV);
    int V[10];
    int H[18];
    short int q[10][17];
protected:
    virtual void keyPressEvent(QKeyEvent *event);
    virtual void keyReleaseEvent(QKeyEvent* event);
private:
   int key;
};

#endif // GENWINDOW

