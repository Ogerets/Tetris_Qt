#ifndef FSNAKEL_H
#define FSNAKEL_H

#include "figure.h"

class fSnakeL_h : public  Figure
{
    Q_OBJECT

public:
    fSnakeL_h(QWidget *parent=0);
    virtual bool BoamR();
    virtual bool BoamL();
    virtual void Trans();
public slots:
    virtual void MoveB();
};

#endif // FSNAKEL_H

