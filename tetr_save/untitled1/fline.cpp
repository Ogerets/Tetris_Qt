#include "fLine.h"
#include "genwindow.h"

extern GenWindow *win;

fLine::fLine(QWidget *parent) : Figure(parent)
{
    posV=-1;
    pol = qrand()%2;
    m = (Cube **)malloc(4*sizeof(Cube));

    posX=100;
    posY=-100;
    posH=4;
    setGeometry(posX,posY, 25, 100);
    for (int i=0;i<4;i++)
    {
        m[i] = new Cube(this);
        m[i]->show();
        m[i]->posX=0;
        m[i]->posY=i*25;
        m[i]->setGeometry(m[i]->posX,m[i]->posY,25,25);
    }
}


void fLine::MoveB()
{
    posY++;

    for (int i=-2;i<16;i++)
    if (posY==i*25) {posV=i+1; break;}

    setGeometry(posX,posY, width(), height());


    //QMessageBox::information(this,QString::number(win->f->posX),QString::number(win->f->posY));
    if ((win->q[posH][posV+4]>=0) || (posV==13))
    {
         QObject::disconnect(win->tmr,SIGNAL(timeout()),this,SLOT(MoveB()));
         win->V[posH]=win->V[posH]+4;

         if (posV<=0) {QMessageBox::information(this,"Tetris","Game Over!"); win->close(); return;}

         win->locat(0,0);

         win->locat(0,1);

         win->locat(0,2);

         win->locat(0,3);

         delete(this);
         win->nweC();
    }
}

void fLine::Trans()
{  
    if ((posH>=2) && (posH<=8) && (win->q[posH-1][posV+3]==-1) && (win->q[posH-2][posV+3]==-1) && (win->q[posH+1][posV+3]==-1) /*&& (win->q[posH+2][posV+3]==-1)*/)
    {
        QObject::disconnect(win->tmr,SIGNAL(timeout()),this,SLOT(MoveB()));
        int x=posX-50;
        int y=posY+50;
        int h=posH-2;
        int v=posV+2;
        delete(this);
        win->f = new fLine_h(win);
        win->f->show();
        win->f->posX=x;
        win->f->posY=y;
        win->f->posH=h;
        win->f->posV=v;
        win->f->setGeometry(x,y,win->f->width(),win->f->height());
        QObject::connect(win->tmr,SIGNAL(timeout()),win->f,SLOT(MoveB()));

        //win->type = 5;
        //win->createf(posX-50,posY+50,posV+2,posH-2);
    }
}

bool fLine::BoamR()
{
    if ((win->q[posH+1][posV+4]<0) && (win->q[posH+1][posV+3]<0) && (win->q[posH+1][posV+2]<0)) return 1;
    return 0;
}

bool fLine::BoamL()
{
    if ((win->q[posH-1][posV+4]<0) && (win->q[posH-1][posV+3]<0) && (win->q[posH-1][posV+2]<0)) return 1;
    return 0;
}
