#ifndef FCUBE
#define FCUBE

#include "figure.h"

class fCube : public Figure
{
    Q_OBJECT

public:
    fCube(QWidget *parent =0);
    virtual void Trans(){}
    virtual bool BoamR();
    virtual bool BoamL();
public slots:
    virtual void MoveB();
};

#endif // FCUBE

