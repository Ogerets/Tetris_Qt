#ifndef CUBE
#define CUBE


#include <QLabel>
#include <QWidget>
#include <QPainter>
//#include "GenWindow.h"

class QPaintEvent;

class Cube : public  QWidget
{
    Q_OBJECT

public:
    Cube(QWidget *parent=0);
    int posX;
    int posY;
    int posH;
    int posV;
private:
    QPainter *q;
    QLabel *h;
protected:
    void paintEvent(QPaintEvent *);

};

#endif // CUBE

