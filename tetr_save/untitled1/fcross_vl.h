#ifndef FCROSS_VL
#define FCROSS_VL

#include "figure.h"

class fCross_vl : public Figure
{
    Q_OBJECT

public:
    fCross_vl(QWidget *parent =0);
    virtual void Trans();
    virtual bool BoamR();
    virtual bool BoamL();
public slots:
    virtual void MoveB();
};

#endif // FCROSS_VL

