#ifndef FHORSEL_VL
#define FHORSEL_VL

#include "figure.h"

class fHorseL_vl : public  Figure
{
    Q_OBJECT

public:
    fHorseL_vl(QWidget *parent=0);
    virtual bool BoamR();
    virtual bool BoamL();
    virtual void Trans();
public slots:
    virtual void MoveB();
};

#endif // FHORSEL_VL

