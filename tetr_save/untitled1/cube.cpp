#include "Cube.h"
#include "GenWindow.h"
#include <QKeyEvent>
#include <QMessageBox>
#include <QPainter>

extern GenWindow *win;

Cube::Cube(QWidget *parent):QWidget(parent)
{
    //active=true;
    setFocusPolicy(Qt::StrongFocus);
    posX=100;
    posY=-25;
    //posX=0;
    //posY=0;
    posH = 4;
    posV = 0;
    setGeometry(posX,posY, 25, 25);
    //repaint();
}

void Cube::paintEvent(QPaintEvent *) {
    QPainter p(this); // Создаём новый объект рисовальщика
    p.setPen(QPen(Qt::red,1,Qt::SolidLine)); // Настройки рисования
    //p.drawLine(0,0,width(),height()); // Рисование линии
    //p.drawRect(0, 0, width()-1, height()-1);
    p.fillRect(0, 0, width()-1, height()-1, Qt::red);
}

