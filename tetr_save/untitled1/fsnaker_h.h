#ifndef FSNAKER_H
#define FSNAKER_H

#include "figure.h"

class fSnakeR_h : public Figure
{
    Q_OBJECT

public:
    fSnakeR_h(QWidget *parent=0);
    virtual bool BoamR();
    virtual bool BoamL();
    virtual void Trans();
public slots:
    virtual void MoveB();
};

#endif // FSNAKER_H

