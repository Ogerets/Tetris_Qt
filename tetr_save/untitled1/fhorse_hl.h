#ifndef FHORSE_HL
#define FHORSE_HL

#include "figure.h"

class fHorseL_hl : public  Figure
{
    Q_OBJECT

public:
    fHorseL_hl(QWidget *parent=0);
    virtual bool BoamR();
    virtual bool BoamL();
    virtual void Trans();
public slots:
    virtual void MoveB();
};

#endif // FHORSE_HL

