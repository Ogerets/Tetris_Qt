#-------------------------------------------------
#
# Project created by QtCreator 2015-10-10T21:50:01
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = untitled1
TEMPLATE = app


SOURCES += main.cpp \
    genwindow.cpp \
    cube.cpp \
    fcube.cpp \
    fline.cpp \
    fsnakel.cpp \
    fsnaker.cpp \
    fhorsel.cpp \
    fline_h.cpp \
    fsnakel_h.cpp \
    fsnaker_h.cpp \
    fhorsel_hr.cpp \
    fhorsel_vl.cpp \
    fhorse_hl.cpp \
    fcross.cpp \
    fcross_vr.cpp \
    fcross_hd.cpp \
    fcross_vl.cpp

HEADERS  += \
    genwindow.h \
    cube.h \
    fcube.h \
    fsnakel.h \
    fsnaker.h \
    figure.h \
    fhorsel.h \
    fline.h \
    fline_h.h \
    fsnakel_h.h \
    fsnaker_h.h \
    fhorsel_hr.h \
    fhorsel_vl.h \
    fhorse_hl.h \
    fcross.h \
    fcross_vr.h \
    fcross_hd.h \
    fcross_vl.h
