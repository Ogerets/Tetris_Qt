#include "fLine.h"
#include "genwindow.h"

extern GenWindow *win;

fLine_h::fLine_h(QWidget *parent) : Figure(parent)
{
    posV=-1;
    m = (Cube **)malloc(4*sizeof(Cube));

    posX = 50;
    posY = -25;
    posH=2;
    setGeometry(posX,posY, 100, 25);
    for (int i=0;i<4;i++)
    {
        m[i] = new Cube(this);
        m[i]->show();
        m[i]->posX=i*25;
        m[i]->posY=0;
        m[i]->setGeometry(m[i]->posX,m[i]->posY,25,25);
    }
}


void fLine_h::MoveB()
{
    posY++;

    for (int i=-2;i<16;i++)
    if (posY==i*25) {posV=i+1; break;}

    setGeometry(posX,posY, width(), height());


    if ((win->q[posH][posV+1]>=0) || (win->q[posH+1][posV+1]>=0) || (win->q[posH+2][posV+1]>=0) ||  (win->q[posH+3][posV+1]>=0) || (posV==16))
    {
        //QMessageBox::information(this,"QString::number(posH)","QString::number(posV)");
        QObject::disconnect(win->tmr,SIGNAL(timeout()),this,SLOT(MoveB()));
        win->V[posH]++;
        win->V[posH+1]++;
        win->V[posH+1]++;
        win->V[posH+1]++;

        if (posV<=0) {QMessageBox::information(this,"Tetris","Game Over!"); win->close(); return;}

        win->locat(0,0);

        win->locat(1,0);

        win->locat(2,0);

        win->locat(3,0);

        delete(this);
        win->nweC();
    }
}

void fLine_h::Trans()
{
    if ((posV<=14) && (win->q[posH+2][posV+1]==-1) && (win->q[posH+2][posV]==-1) && (win->q[posH+2][posV-1]==-1))
    {
        QObject::disconnect(win->tmr,SIGNAL(timeout()),this,SLOT(MoveB()));
        int x=posX+50;
        int y=posY-50;
        int h=posH+2;
        int v=posV-2;
        delete(this);
        win->f = new fLine(win);
        win->f->show();
        win->f->posX=x;
        win->f->posY=y;
        win->f->posH=h;
        win->f->posV=v;
        win->f->setGeometry(x,y,win->f->width(),win->f->height());
        QObject::connect(win->tmr,SIGNAL(timeout()),win->f,SLOT(MoveB()));

        //win->type = 1;
        //win->createf(posX+50,posY-50,posV-2,posH+2);
    }
}

bool fLine_h::BoamR()
{
    if (win->q[posH+4][posV+1]<0) return 1;
    return 0;
}

bool fLine_h::BoamL()
{
   if (win->q[posH-1][posV+1]<0) return 1;
   return 0;
}

