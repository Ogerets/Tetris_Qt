#ifndef FCROSS_HD
#define FCROSS_HD

#include "figure.h"

class fCross_hd : public Figure
{
    Q_OBJECT

public:
    fCross_hd(QWidget *parent =0);
    virtual void Trans();
    virtual bool BoamR();
    virtual bool BoamL();
public slots:
    virtual void MoveB();
};

#endif // FCROSS_HD

