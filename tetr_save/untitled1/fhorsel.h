#ifndef FHORSEL
#define FHORSEL

#include "figure.h"

class fHorseL : public  Figure
{
    Q_OBJECT

public:
    fHorseL(QWidget *parent=0);
    virtual bool BoamR();
    virtual bool BoamL();
    virtual void Trans();
public slots:
    virtual void MoveB();
};

#endif // FHORSEL

